﻿const config = require('config.js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const User = db.User;
const UserLog = db.UserLog;
const mongoose = require('mongoose');
module.exports = {
    authenticate,
    getAll,
    getAuditAll,
    getById,
    create,
    update,
    logout,
    delete: _delete
};
var ObjectId = mongoose.Types.ObjectId;
async function authenticate({ username, password ,ip}) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret);
        let logParams = {
            "userId":user.toObject()._id,
            "loginTime": new Date(),
            "clientIp" : ip
        }
        const userLog = new UserLog(logParams);
        await userLog.save();
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}
async function getAuditAll() {
    return  await User.aggregate([{
        $lookup:{
            from: UserLog.collection.name,
            localField:"_id",
            foreignField:"userId",
            as:"logs"
        }
    }]);
    
}
async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}
async function logout(id){
    return await UserLog.updateMany({userId : ObjectId(id),logoutTime : null},{logoutTime: new Date()},{ multi: true });
    
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}