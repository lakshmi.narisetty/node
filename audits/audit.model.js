const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    userId: { type:  Schema.Types.ObjectId, required: true },
    loginTime: { type: Date },
    logoutTime: { type: Date},
    clientIp: { type: String, required: true },
    createdDate: { type: Date, default: Date.now  }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Usersaudit', schema);